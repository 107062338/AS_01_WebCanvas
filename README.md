# AS_01_WebCanvas

## initial commit
*  2020/04/08
*  Empty project

## v1
*  2020/04/11
*  Finish Basic elements
*  Without CSS

## v2
*  2020/04/12
*  color picker(library)
*  CSS finished

### v2.1
*  deal with path problems

### v2.2
*  deal with javascript path problem

### v2.3
*   update README.md

## v3
*   finish

### v3.1
*   add background-image

### v3.2
*   add color picker

### v3.3
*   repair BUG
---
| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

## How to use
*   **筆刷、圖型跟文字共用width, color**
*   **橡皮擦大小使用width**
*   **文字方塊使用方法**
    1.  文字方塊使用方法為按下文字方塊按鈕後在canvas中點選要填入文字的位置
    2.  輸入完畢後按下enter結束(必要)
*   **可切換填滿/描邊**<br>
範例圖中color picker已改成調色盤的樣子，並且新增背景圖片
![image](./img/description.png)

## Gitlab page link
https://107062338.gitlab.io/AS_01_WebCanvas
---