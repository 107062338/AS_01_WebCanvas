var operation = "pen";
canvas.style.cursor = "url('./img/pen_1.cur'), auto";

//Basic
function PEN(){
    operation = "pen";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/pen_1.cur'), auto";
  }
  function LINE(){
    operation = "line";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/line.cur'), auto";
  }
  function RECT(){
    operation = "rect";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/rect.cur'), auto";
  }
  function CIRCLE(){
    operation = "circle";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/circle.cur'), auto";
  }
  function TRIANGLE(){
    operation = "triangle";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/triangle.cur'), auto";
  }
  function ERASER(){
    operation = "eraser";
    ctx.globalCompositeOperation = 'destination-out';
    canvas.style.cursor = "url('./img/eraser.cur'), auto";
  }
  function FILL(){
    if(filled){
      filled = false;
      document.getElementById("btn-fill").innerHTML = "<img src=\"./img/w_fill.png\">";
    }
    else{
      filled = true;
      document.getElementById("btn-fill").innerHTML = "<img src=\"./img/w_filled.png\">";
    }
  }
  // implement text
  function TEXT(){
    operation = "text";
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "url('./img/text.cur'), auto";
  }

  function REDRAW(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    cPush();
  }
  function UNDO(){
    if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height); 
        if(ctx.globalCompositeOperation == 'destination-out'){
          ctx.globalCompositeOperation = 'source-over';
          ctx.drawImage(canvasPic, 0, 0); 
        }
        else{
          ctx.drawImage(canvasPic, 0, 0); 
        }
      }
    }
  }
  function REDO(){
    if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.clearRect(0, 0, canvas.width, canvas.height); ctx.drawImage(canvasPic, 0, 0); }
    }
  }
  function SAVE(){
    var download = document.getElementById("download");
    var image = document.getElementById("canvas").toDataURL("image/png")
                .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
  }