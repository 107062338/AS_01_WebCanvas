//canvas initialize
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var color_canvas = document.getElementById('color_canvas');
var color_ctx = color_canvas.getContext('2d');
var Cinfo = canvas.getBoundingClientRect();
var cPushArray = new Array();
var cStep = -1;
var firstmousePos;
var filled = false;
var imageLoader = document.getElementById('imageLoader');
var input = document.getElementById("input");
document.getElementById('whole').style.width = window.innerWidth-50 + "px";
document.getElementById('whole').style.height = window.innerHeight-150 + "px";
input.style.display = "none";
cPush();
color_init();

color_canvas.addEventListener('mousedown', function(event){
  var choose = color_ctx.getImageData(event.x - 764, 20, 1, 1);
  document.getElementById('example').style.backgroundColor = 'rgb(' + choose.data[0] + ', ' + choose.data[1] + ', ' + choose.data[2] + ')';
  ctx.strokeStyle = 'rgb(' + choose.data[0] + ', ' + choose.data[1] + ', ' + choose.data[2] + ')'; 
  ctx.fillStyle = 'rgb(' + choose.data[0] + ', ' + choose.data[1] + ', ' + choose.data[2] + ')'; 
});
imageLoader.addEventListener('change', handleImage, false);

canvas.addEventListener('click', function(event){if(operation !== "text")cPush();});

canvas.addEventListener('mousedown', function(evt) {
    firstmousePos = getMousePos(canvas, evt);
    if(operation === "pen" || operation === "eraser") ctx.beginPath();
    if(operation === "text"){
      input.style.display = "initial";
      input.style.left = evt.x + "px";
      input.style.top = evt.y + "px";
      document.addEventListener('keydown', finish_text);
    }
    canvas.addEventListener('mousemove', mouseMove, false);
});

document.addEventListener('mouseup', function(event) {
  canvas.removeEventListener('mousemove', mouseMove, false);
  if(operation === "pen" || operation === "eraser") ctx.closePath();
  document.dispatchEvent(new Event('click'));
}, false);

function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    var canvasPic = new Image();
    canvasPic.src = cPushArray[cStep];  
    if(operation === "pen"){
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
    }
    if(operation === "line"){
      ctx.beginPath();
      canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvasPic, 0, 0); 
        ctx.moveTo(firstmousePos.x, firstmousePos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.closePath();
        ctx.stroke();
      }
    }    
    else if(operation === "rect"){
      ctx.beginPath();
      canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvasPic, 0, 0); 
        if(filled)  ctx.fillRect(firstmousePos.x, firstmousePos.y, mousePos.x-firstmousePos.x, mousePos.y-firstmousePos.y);
        else  ctx.strokeRect(firstmousePos.x, firstmousePos.y, mousePos.x-firstmousePos.x, mousePos.y-firstmousePos.y);
        ctx.closePath();
      }
    }  
    else if(operation === "circle"){
      ctx.beginPath();
      canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height); 
        ctx.drawImage(canvasPic, 0, 0); 
        ctx.arc(firstmousePos.x, firstmousePos.y, Math.sqrt(Math.pow(mousePos.x-firstmousePos.x, 2)+Math.pow(mousePos.y-firstmousePos.y, 2)), 0, 2 * Math.PI, true);
        ctx.closePath();
        if(filled)  ctx.fill();
        else  ctx.stroke();
      }
    }
    else if(operation === "triangle"){
      ctx.beginPath();
      canvasPic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height); 
        ctx.drawImage(canvasPic, 0, 0); 
        ctx.moveTo(firstmousePos.x, firstmousePos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.lineTo(firstmousePos.x - (mousePos.x-firstmousePos.x), mousePos.y);
        ctx.closePath();
        if(filled) ctx.fill();
        else ctx.stroke();
      }
    }
    else if(operation === "eraser"){
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
    }
};

// implement UN/REDO, REDRAW
function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(document.getElementById('canvas').toDataURL());
}


// Upload

function handleImage(e){
  var reader = new FileReader();
  reader.onload = function(event){
      var img = new Image();
      img.onload = function(){
          ctx.drawImage(img,0,0);
          cPush();
      }
      img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);     
}

function info_update(){ 
  ctx.font = document.getElementById('strokewidth').value + 'px '+ document.getElementById('font_family').value;
  ctx.lineWidth = document.getElementById('strokewidth').value;
  document.getElementById("demo").innerHTML = ctx.lineWidth;
}

function color_init(){
  color_cho = color_ctx.createLinearGradient(0, 0, 240, 0);
  color_cho.addColorStop(0, "rgb(0,0,0)");
  color_cho.addColorStop(1 / 8, "rgb(0,255,0)");
  color_cho.addColorStop(2 / 8, "rgb(0,0,255)");
  color_cho.addColorStop(3 / 8, "rgb(0,255,255)");
  color_cho.addColorStop(4 / 8, "rgb(255,0,0)");
  color_cho.addColorStop(5 / 8, "rgb(255,255,0)");
  color_cho.addColorStop(6 / 8, "rgb(255,0,255)");
  color_cho.addColorStop(7 / 8, "rgb(255,255,255)");
  color_cho.addColorStop(1, "rgb(0,0,0)");
  color_ctx.fillStyle = color_cho;
  color_ctx.fillRect(0, 0, 200, 40);
}

function getMousePos(canvas, evt) {
  return {
    x: evt.clientX - Cinfo.left,
    y: evt.clientY - Cinfo.top
  };   
};

function finish_text(event){
  if(event.keyCode == 13 && input.value != ""){
    ctx.fillText(input.value, firstmousePos.x, firstmousePos.y);
    input.value = "";
    input.style.display = "none";
    operation = "pen";
    canvas.style.cursor = "url('./img/pen_1.cur'), auto";
    cPush();
  }
}